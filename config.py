#
# For instructions on how to find these values, read the Readme.md file
#

# Credentials

username = "" # Bot's username (for logging into reddit)
password = "" # Bot's password (for logging into reddit)
client_id = "" # Bot's client ID
client_secret = "" # Bot client secret
user_agent = "redditbot" # User agent, how your script identifies itself

# Reposting instructions

sub_from = "" # The sub from which to retrieve posts
sub_to = "" # The sub on which you want to create posts.

# Configuration settings

max_posts = 5 # The amount of posts to retrieve per run
link_sauce = 0 # Create an automated link to the source post on links
text_sauce = 1 # Create an automated link to the source post on text posts