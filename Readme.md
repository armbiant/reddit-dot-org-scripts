
# Reddit Reposter bot

Ever find a subreddit you like, but it was run by over-censorious mods? Don't you think you could do a better job? Of course you could!

Making a clone subreddit is a bit disheartening. Without any content, nobody is going to make the switch. After all, most people prefer an active subreddit to a rule-less one.

**This bot takes posts from the /new/ feed and reposts them to a subreddit of your choice.** 

That way, you can have all the content without the previous mod team.

## Step 1: Creating your bot account

### Create a new reddit account
In order to make posts you need an account with which to make them. You can use your normal reddit account, but it is courtesy to create a new one with 'bot' in the name to let people know that it's a bot.

### Use the account to create an app
Once your bot account is made and you're logged in, you need to [visit this link](https://www.reddit.com/prefs/apps/) and create a new application.

You'll get a few options to create it, but most of it is arbitrary.
* Name: The name of your application.
* Type: Select 'script'
* Description: Describe your app
* About URI: You can link to here if you want
* Redirect URI: Link here as well if you'd like

Once you create it, you'll get a few values that you'll want to take note of:
* Client ID: Located right under 'personal use script'
* Client Secret: Right under that, next to its label

## Step 2: Configure your settings

Open *config.py* and edit the settings.

### Login credentials
* **Username:** Your bot's login username
* **Password:** Your bot's login password 
* **Client_id and client_secret:** These are the values you wrote down after creating your app in Step 1
* **User_agent:** This is how your bot identifies itself to Reddit. It can be anything arbitrary, but it's best to make it unique to your script.

### Subreddit settings
* **Sub_from:** This is the subreddit that you're copying the posts from
* **Sub_to:** This is the target sub on which you will be reposting them. Make sure that your bot has permission to create posts. Making them an approved user or mod can help make it easier.

### Misc. configuration
* **Max_posts:** This is the amount of posts to check and pull on each run. A lower number runs faster, but runs the risk of creating reposts if the mods in the source subreddit delete a lot of posts very quickly.
* **Link_sauce and text_sauce:** If set to true, the bot will create an automated reply to the post they created with a link to the source post. This will help people find the original post to see what replies they got if its as

## Step 3: Set up your Python sript
Now you need the thing to run. You'll need some kind of computing power to set this up, I got it up and running on an AWS EC2 Linux/Ubuntu server, but it should be able to run anywhere where python runs.

### Install Python

First of all, you'll need to make sure your machine has python 3 installed. Praw, the library that allows us to integrate with Reddit, only works with Python 3. First check to see if you have it running:

    python3 --version

If you get an error, it means it's not installed correctly. In which case, run the two following commands:

    sudo apt-get update
    sudo apt-get install python3

You might have to use a different install command depending on what operating system you're using.

### Install dependencies

Install Python Pip (Python depenency manager thingy):

    sudo apt-get install python3-pip

Install Praw (reddit API library for Python):

    sudo pip3 install praw

## Step 4: Run your script!

That's all set! Now you can test your script.

    python3 bot.py

And you should get an output that looks like this:

	Connected to account
	Retrieved 5 posts from /r/askreddit.
	Output:
	Retrieved 20 posts from /r/askreddit.
	Posts posted: 5
	Finished

The first "retrieved" is the posts that it is pulling for posting to the target sub. The second line is retrieving four times as much so that it can check for any reposts.

## Step 5: Cron jobs (Optional)

If you're going to be using this for a real subreddit, I recommend that you set up a cron job and use it to run the above `python3 bot.py` command on a regular schedule. Now, the frequency depends on how active the sub is at its peak so there is no best practice. Play around with the Cron frequency and the max_posts setting until you get it right.

Have fun!

[Made by N64 Squid](https://n64squid.com/reddit-reposting-bot/)