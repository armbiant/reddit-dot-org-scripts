import praw
import config
import json

sub_from = config.sub_from
sub_to = config.sub_to

class postClass:
	global sub_from
	global sub_to
	title = ''
	link = 0
	url = ''
	body = ''
	exists = 0
	def __repr__(self):
		 return "<Title:%s Link:%d URL:%s>" % (self.title, self.link, self.url)
	source_sub = sub_from
	target_sub = sub_to

def bot_login():
	r = praw.Reddit(username = config.username,
			password = config.password,
			client_id = config.client_id,
			client_secret = config.client_secret,
			user_agent = config.user_agent)
	print("Connected to account")
	return r

def run_bot(r,sub,maxposts=config.max_posts):
	output = []
	for post in r.subreddit(sub).new(limit=maxposts):
		out = postClass()
		out.title = post.title
		out.nsfw = post.over_18
		if "reddit.com" in post.url:
			out.link = 0
			out.body = post.selftext
		else:
			out.link = 1
			out.url = post.url
		out.author = post.author
		out.permalink = post.permalink
		output.append(out)
	print("Retrieved "+str(len(output))+" posts from /r/"+sub+".")
	return output

# Run the above functions
r = bot_login()
newposts = run_bot(r, config.sub_from)
print('Output:')
currentposts = run_bot(r,config.sub_to,config.max_posts*4)
for k,new in enumerate(newposts):
	for old in currentposts:
		if new.title == old.title:
			newposts[k].exists = 1
# Create new posts
totalposted = 0
for new in newposts:
	if new.exists:
		continue
	else:
		print("Let's post: " + new.title)
	if new.link:
		newpost = r.subreddit(config.sub_from).submit(title=new.title, url=new.url, nsfw=new.nsfw)
		if config.link_sauce:
			newpost.reply("Sauce: "+new.author.name+"\n\n"+new.permalink)
	else:
		newpost = r.subreddit(config.sub_from).submit(title=new.title, selftext=new.body, nsfw=new.nsfw)
		# Create a reply with the source
		if config.text_sauce:
			newpost.reply("Sauce: "+new.author.name+"\n\n"+new.permalink)
	totalposted += 1;

print("Posts posted: "+str(totalposted))
print('Finished')
